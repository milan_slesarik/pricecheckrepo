# -*- coding: utf-8 -*- 
from django.db.models.signals import post_save
from django.dispatch import receiver
import models

@receiver(post_save,sender=models.Product)
def set_uncategorized_after_creating_product(sender,created,instance,**kwargs):
    if created:
        uncategorized = models.Category.objects.get_or_create(name='Uncategorized')[0]
        instance.category = uncategorized
        instance.save()


