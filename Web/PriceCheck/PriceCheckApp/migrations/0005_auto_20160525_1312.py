# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-25 11:12
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PriceCheckApp', '0004_auto_20160525_1310'),
    ]

    operations = [
        migrations.RenameField(
            model_name='exchangerate',
            old_name='exchange_rate',
            new_name='exchange_rate_multiplier',
        ),
    ]
