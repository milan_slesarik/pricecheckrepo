from __future__ import unicode_literals
from django.
from django.db import models

class ServiceManager(models.Model):
    """
    """
    user = models.OneToOneField(User)

class Category(models.Model):
    name = models.CharField(max_length=100)

class Product(models.Model):
    """ Any product ( f.e. 'LG P550' )
        Can have multiple occurencies (Alza.cz,Heureka.sk....)
    """
    name = models.CharField(max_length=200)
    groups = models.ManyToManyField('Group', related_name='products',blank=True,null=True)
    category = models.ForeignKey('Category', related_name='products',null=True,blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Products ()'

class Occurence(models.Model):
    product = models.ForeignKey(Product, related_name='occurences', on_delete=models.CASCADE)
    url = models.TextField()
    xpath = models.TextField()
    currency = models.ForeignKey('Currency', related_name='occurences', null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Occurencies'

    def __unicode__(self):
        return self.product.__unicode__() + ' occurences'


class Scan(models.Model):
    datetime = models.DateTimeField()
    price = models.FloatField()
    valid = models.BooleanField(default=True)


class Group(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name



class Checking(models.Model):
    frequency = models.PositiveIntegerField()
    active = models.BooleanField(default=True)
    last_scan = models.OneToOneField('Scan')


class Currency(models.Model):
    shortcut = models.CharField(max_length=20)

    class Meta:
        verbose_name_plural = "Currencies"

    def __unicode__(self):
        return self.shortcut


class ExchangeRate(models.Model):
    currency_from = models.ForeignKey(Currency, related_name='currency_from')
    currency_to = models.ForeignKey(Currency, related_name='currency_to')
    exchange_rate_multiplier = models.FloatField()

    def __unicode__(self):
        return '{} to {}'.format(self.currency_from, self.currency_to)

import signals