# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-25 10:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('PriceCheckApp', '0002_auto_20160525_1248'),
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('shortcut', models.CharField(max_length=20)),
                ('exchange_rate', models.FloatField()),
            ],
        ),
        migrations.AddField(
            model_name='occurence',
            name='currency',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='occurences', to='PriceCheckApp.Currency'),
        ),
    ]
