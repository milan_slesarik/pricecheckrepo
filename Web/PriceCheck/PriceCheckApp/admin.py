from django.contrib import admin
import models

admin.site.register(models.Group)
admin.site.register(models.Checking)
admin.site.register(models.Scan)
admin.site.register(models.Product)
admin.site.register(models.Occurence)
admin.site.register(models.Currency)
admin.site.register(models.ExchangeRate)
admin.site.register(models.Category)